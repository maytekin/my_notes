------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   DynamoDB Streams help you to keep a list of item level changes or provide a list of item level changes that have taken place in the last 24hrs. 
    Amazon DynamoDB is integrated with AWS Lambda so that you can create triggers—pieces of code that automatically respond to events in DynamoDB Streams.

    If you enable DynamoDB Streams on a table, you can associate the stream ARN with a Lambda function that you write. 
    Immediately after an item in the table is modified, a new record appears in the table’s stream. AWS Lambda polls the stream and 
    invokes your Lambda function synchronously when it detects new stream records.

    An event source mapping identifies a poll-based event source for a Lambda function. It can be either an Amazon Kinesis or DynamoDB stream. 
    Event sources maintain the mapping configuration except for stream-based services (e.g. DynamoDB, Kinesis) for which the configuration is made 
    on the Lambda side and Lambda performs the polling.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   ElastiCache is a web service that makes it easy to deploy and run Memcached or Redis protocol-compliant server nodes in the cloud. The in-memory caching provided 
    by ElastiCache can be used to significantly improve latency and throughput for many read-heavy application workloads or compute-intensive workloads

    RDS is not the optimum solution due to the requirement to optimize retrieval times which is a better fit for an in-memory data store such as ElastiCache.

    Kinesis Data Streams is used for processing streams of data, it is not a persistent data store.

    Memcached does not offer persistence.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazon DynamoDB Accelerator (DAX) is a fully managed, highly available, in-memory cache for DynamoDB that delivers up to a 
    10x performance improvement – from milliseconds to microseconds – even at millions of requests per second. You can enable DAX for a DynamoDB database with a few clicks.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-    If the master and Read Replica are in different regions, you encrypt using the encryption key for that region.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Periodically, Amazon RDS performs maintenance on Amazon RDS resources. Maintenance most often involves updates to the DB instance’s underlying hardware, 
    underlying operating system (OS), or database engine version. Updates to the operating system most often occur for security issues and should be done as soon as possible.

    Some maintenance items require that Amazon RDS take your DB instance offline for a short time. Maintenance items that require a resource to be offline 
    include required operating system or database patching. Required patching is automatically scheduled only for patches that are related to security and instance reliability. Such patching occurs infrequently (typically once every few months) and seldom requires more than a fraction of your maintenance window.

    Enabling Multi-AZ, promoting a Read Replica and updating DB parameter groups are not events that take place during a maintenance window.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Aurora cluster volumes automatically grow as the amount of data in your database increases. An Aurora cluster volume can grow to a maximum size of 64 tebibytes (TiB). 
    Table size is limited to the size of the cluster volume. That is, the maximum table size for a table in an Aurora DB cluster is 64 TiB.

    Aurora Replicas are independent endpoints in an Aurora DB cluster, best used for scaling read operations and increasing availability. 
    Up to 15 Aurora Replicas can be distributed across the Availability Zones that a DB cluster spans within an AWS Region. The DB cluster volume is made up of
    multiple copies of the data for the DB cluster. However, the data in the cluster volume is represented as a single, logical volume to the 
    primary instance and to Aurora Replicas in the DB cluster.

    As a result, all Aurora Replicas return the same data for query results with minimal replica lag—usually much less than 100 milliseconds after the primary instance 
    has written an update. Replica lag varies depending on the rate of database change. That is, during periods where a large amount of write operations occur 
    for the database, you might see an increase in replica lag.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazon Redshift is a fast, fully managed, petabyte-scale data warehouse service that makes it simple and cost-effective to efficiently analyze all your data using your 
    existing business intelligence tools. It is optimized for datasets ranging from a few hundred gigabytes to a petabyte or more. Amazon RedShift uses columnar storage.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Connection draining timers are applicable to ELBs not RDS.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   “DynamoDB scales automatically so there’s no need to worry” is incorrect as provisioned capacity mode does not automatically scale.

    Amazon DynamoDB can throttle requests that exceed the provisioned throughput for a table. When a request is throttled it fails with an 
    HTTP 400 code (Bad Request) and a ProvisionedThroughputExceeded exception (not a 503 or 200 status code).

    When using the provisioned capacity pricing model DynamoDB does not automatically scale. 
    DynamoDB can automatically scale when using the new on-demand capacity mode, however this is not configured for this database.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazon Aurora Global Database is designed for globally distributed applications, allowing a single Amazon Aurora database to span multiple AWS regions. 
    It replicates your data with no impact on database performance, enables fast local reads with low latency in each region, and provides disaster recovery 
    from region-wide outages.

    Aurora Global Database uses storage-based replication with typical latency of less than 1 second, using dedicated infrastructure that leaves your database 
    fully available to serve application workloads. In the unlikely event of a regional degradation or outage, one of the secondary regions can be promoted 
    to full read/write capabilities in less than 1 minute.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   DynamoDB best practices include:

        Keep item sizes small.
        If you are storing serial data in DynamoDB that will require actions based on data/time use separate tables for days, weeks, months.
        Store more frequently and less frequently accessed data in separate tables.
        If possible compress larger attribute values.
        Store objects larger than 400KB in S3 and use pointers (S3 Object ID) in DynamoDB.


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Continuous replication is not a replication type that is supported by RDS.

    Scheduled replication is not a replication type that is supported by RDS.

    Asynchronous replication is used by RDS for Read Replicas.

    Multi-AZ RDS creates a replica in another AZ and synchronously replicates to it (DR only). Multi-AZ deployments for the MySQL, MariaDB, 
    Oracle and PostgreSQL engines utilize synchronous physical replication. Multi-AZ deployments for the SQL Server engine use synchronous logical 
    replication (SQL Server-native Mirroring technology).

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazon S3 Select is designed to help analyze and process data within an object in Amazon S3 buckets, faster and cheaper. 
    It works by providing the ability to retrieve a subset of data from an object in Amazon S3 using simple SQL expressions

    Amazon Redshift Spectrum allows you to directly run SQL queries against exabytes of unstructured data in Amazon S3. No loading or transformation is required.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   You cannot restore from a DB snapshot to an existing DB – a new instance is created when you restore.
    RDS uploads transaction logs for DB instances to Amazon S3 every 5 minutes.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   A VPC endpoint enables you to privately connect your VPC to supported AWS services and VPC endpoint services powered by AWS PrivateLink 
    without requiring an internet gateway, NAT device, VPN connection, or AWS Direct Connect connection.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Redis authentication tokens enable Redis to require a token (password) before allowing clients to execute commands, thereby improving data security.

    You can require that users enter a token on a token-protected Redis server. To do this, include the parameter –auth-token (API: AuthToken) with the correct 
    token when you create your replication group or cluster. Also include it in all subsequent commands to the replication group or cluster.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazon ElastiCache in-transit encryption is an optional feature that allows you to increase the security of your data at its most vulnerable 
    points—when it is in transit from one location to another. Because there is some processing needed to encrypt and decrypt the data at the endpoints, 
    enabling in-transit encryption can have some performance impact. You should benchmark your data with and without in-transit encryption to determine the 
    performance impact for your use cases.

    ElastiCache in-transit encryption implements the following features:

        Encrypted connections—both the server and client connections are Secure Socket Layer (SSL) encrypted.
        Encrypted replication—data moving between a primary node and replica nodes is encrypted.
        Server authentication—clients can authenticate that they are connecting to the right server.
        Client authentication—using the Redis AUTH feature, the server can authenticate the clients.


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazon ElastiCache with the Memcached engine is an in-memory database that can be used as a database caching layer. 
    The memached engine supports multiple cores and threads and large nodes.

    The Redis engine does not support multiple CPU cores or threads.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazond DynamoDB ==> Using AWS Lambda to modify the provisioned throughput is possible but it would be more cost-effective to use DynamoDB Auto Scaling 
                         as there is no cost to using it.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazon Kinesis Firehose processes streaming data, not data stored on S3.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   AWS Batch is used for running batch computing jobs across a fleet of EC2 instances.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Some applications, such as media catalog updates require high frequency reads, and consistent throughput. 
    For such applications, customers often complement S3 with an in-memory cache, such as Amazon ElastiCache for Redis, 
    to reduce the S3 retrieval cost and to improve performance.

    ElastiCache for Redis is a fully managed, in-memory data store that provides sub-millisecond latency performance with high throughput. 
    ElastiCache for Redis complements S3 in the following ways:

        Redis stores data in-memory, so it provides sub-millisecond latency and supports incredibly high requests per second.
        It supports key/value based operations that map well to S3 operations (for example, GET/SET => GET/PUT), making it easy to write code for both S3 and ElastiCache.
        It can be implemented as an application side cache. This allows you to use S3 as your persistent store and benefit from its durability, availability, and low cost. 
        Your applications decide what objects to cache, when to cache them, and how to cache them.

    In this example the media catalog is pulling updates from S3 so the performance between these components is what needs to be improved. 
    Therefore, using ElastiCache to cache the content will dramatically increase the performance.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Aurora Replicas are independent endpoints in an Aurora DB cluster, best used for scaling read operations and increasing availability. 
    Up to 15 Aurora Replicas can be distributed across the Availability Zones that a DB cluster spans within an AWS Region.

    The DB cluster volume is made up of multiple copies of the data for the DB cluster. However, the data in the cluster volume is represented as a single, 
    logical volume to the primary instance and to Aurora Replicas in the DB cluster.

    An Aurora Replica is both a standby in a Multi-AZ configuration and a target for read traffic. The architect simply needs to direct traffic to the Aurora Replica.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   If you don’t know the performance requirements it will be difficult to determine the correct instance type to use. Amazon Aurora Serverless does not require
    you to make capacity decisions upfront as you do not select an instance type. As a serverless service it will automatically scale as needed.

    Amazon Aurora Serverless is an on-demand, auto-scaling configuration for Amazon Aurora. The database automatically starts up, shuts down, and scales capacity 
    up or down based on application needs. This is an ideal database solution for infrequently-used applications.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazon Aurora Global Database is designed for globally distributed applications, allowing a single Amazon Aurora database to span multiple AWS regions. 
    It replicates your data with no impact on database performance, enables fast local reads with low latency in each region, and provides disaster recovery from region-wide outages

    If your primary region suffers a performance degradation or outage, you can promote one of the secondary regions to take read/write responsibilities. 
    An Aurora cluster can recover in less than 1 minute even in the event of a complete regional outage.

    This provides your application with an effective Recovery Point Objective (RPO) of 1 second and a Recovery Time Objective (RTO) of less than 1 minute, 
    providing a strong foundation for a global business continuity plan

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Aurora Global Database lets you easily scale database reads across the world and place your applications close to your users. 
    Your applications enjoy quick data access regardless of the number and location of secondary regions, with typical cross-region replication latencies below 1 second.

    If your primary region suffers a performance degradation or outage, you can promote one of the secondary regions to take read/write responsibilities. 
    An Aurora cluster can recover in less than 1 minute even in the event of a complete regional outage. 
    This provides your application with an effective Recovery Point Objective (RPO) of 1 second and a Recovery Time Objective (RTO) of less than 1 minute, 
    providing a strong foundation for a global business continuity plan.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   ElastiCache can be used to cache requests to an Amazon RDS database through application configuration. 
    This can greatly improve performance as ElastiCache can return responses to queries with sub-millisecond latency.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   You can specify one of the following when initiating a job to retrieve an archive based on your access time and cost requirements.

        Expedited — Expedited retrievals allow you to quickly access your data when occasional urgent requests for a subset of archives are required. 
        For all but the largest archives (250 MB+), data accessed using Expedited retrievals are typically made available within 1–5 minutes. 
        Provisioned Capacity ensures that retrieval capacity for Expedited retrievals is available when you need it.

        Standard — Standard retrievals allow you to access any of your archives within several hours. Standard retrievals typically complete within 3–5 hours. 
        This is the default option for retrieval requests that do not specify the retrieval option.
        
        Bulk — Bulk retrievals are S3 Glacier’s lowest-cost retrieval option, which you can use to retrieve large amounts, even petabytes, of data 
        inexpensively in a day. Bulk retrievals typically complete within 5–12 hours.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-    AWS recommend using General Purpose SSD rather than Throughput Optimized HDD for most use cases but it is more expensive.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   S3 Standard-IA is for data that is accessed less frequently, but requires rapid access when needed. S3 Standard-IA offers the high durability, 
    high throughput, and low latency of S3 Standard, with a low per GB storage price and per GB retrieval fee.

    This combination of low cost and high performance make S3 Standard-IA ideal for long-term storage, backups, and as a data store for disaster recovery files. 
    S3 Storage Classes can be configured at the object level and a single bucket can contain objects stored across S3 Standard, S3 Intelligent-Tiering, 
    S3 Standard-IA, and S3 One Zone-IA.

    You can also use S3 Lifecycle policies to automatically transition objects between storage classes without any application changes.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   The possible values are ok, impaired, warning, or insufficient-data. If all checks pass, the overall status of the volume is ok. If the check fails, 
    the overall status is impaired. If the status is insufficient-data, then the checks may still be taking place on your volume at the time.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Some facts about Amazon EBS encrypted volumes and snapshots:

        All EBS types support encryption and all instance families now support encryption.
        Not all instance types support encryption.
        Data in transit between an instance and an encrypted volume is also encrypted (data is encrypted in trans.
        You can have encrypted an unencrypted EBS volumes attached to an instance at the same time.
        Snapshots of encrypted volumes are encrypted automatically.
        EBS volumes restored from encrypted snapshots are encrypted automatically.
        EBS volumes created from encrypted snapshots are also encrypted.


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   All EBS types and all instance families support encryption but not all instance types support encryption. 
    There is no direct way to change the encryption state of a volume. Data in transit between an instance and an encrypted volume is also encrypted.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   In general, when your object size reaches 100 MB, you should consider using multipart uploads instead of uploading the object in a single operation.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-