------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   An ALB does not support static IP addresses and is not suitable for a proxy function.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   A NAT Gateway is created in a specific AZ and can have a single Elastic IP address associated with it. 
    NAT Gateways are deployed in public subnets and the route tables of the private subnets where the EC2 instances 
    reside are configured to forward Internet-bound traffic to the NAT Gateway. You do pay for using a NAT Gateway based on hourly usage and data processing, 
    however this is still a cost-effective solution.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   The ALB (and NLB) supports IP addresses as targets as well as instance IDs as targets. When you create a target group, you specify its target type, 
    which determines how you specify its targets. After you create a target group, you cannot change its target type.

    Using IP addresses as targets allows load balancing any application hosted in AWS or on-premises using IP addresses of the application back-ends as targets.

    You must have a VPN or Direct Connect connection to enable this configuration to work.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Amazon API Gateway decouples the client application from the back-end application-layer services by providing a single endpoint for API requests.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   An internet gateway is a horizontally scaled, redundant, and highly available VPC component that allows communication between your VPC and the internet.

    An internet gateway serves two purposes: to provide a target in your VPC route tables for internet-routable traffic, and to perform network address translation (NAT) 
    for instances that have been assigned public IPv4 addresses.

    An internet gateway supports IPv4 and IPv6 traffic. It does not cause availability risks or bandwidth constraints on your network traffic.

    NAT gateway does impose a limit of 45 Gbps.

    A VPC endpoint is used to access public services from a VPC without traversing the Internet.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   An Interface endpoint uses AWS PrivateLink and is an elastic network interface (ENI) with a private IP address that serves as an entry point for 
    traffic destined to a supported service.

    Using PrivateLink you can connect your VPC to supported AWS services, services hosted by other AWS accounts (VPC endpoint services), 
    and supported AWS Marketplace partner services.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   A security group acts as a virtual firewall for your instance to control inbound and outbound traffic. When you launch an instance in a VPC, 
    you can assign up to five security groups to the instance. Security groups act at the instance level, not the subnet level. 
    Therefore, each instance in a subnet in your VPC can be assigned to a different set of security groups.

    Network ACL’s function at the subnet level.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Without cross-zone load balancing enabled, the NLB will distribute traffic 50/50 between AZs. As there are an odd number of instances across the two AZs some
    instances will not receive any traffic. Therefore enabling cross-zone load balancing will ensure traffic is distributed evenly between available instances in all AZs.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   When you create a new subnet, it is automatically associated with the main route table

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   The key requirement is to limit the number of requests per second that hit the application. This can only be done by implementing throttling rules on the API Gateway. 
    Throttling enables you to throttle the number of requests to your API which in turn means less traffic will be forwarded to your application server.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   To enable your Lambda function to access resources inside your private VPC, you must provide additional VPC-specific configuration information that 
    includes VPC subnet IDs and security group IDs. AWS Lambda uses this information to set up elastic network interfaces (ENIs) that enable your function.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Alias records are used to map resource record sets in your hosted zone to Amazon Elastic Load Balancing load balancers, API Gateway custom regional 
    APIs and edge-optimized APIs, CloudFront Distributions, AWS Elastic Beanstalk environments, Amazon S3 buckets that are configured as website endpoints, 
    Amazon VPC interface endpoints, and to other records in the same Hosted Zone.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   The company should implement an AWS Direct Connect connection to the closest region. A Direct Connect gateway can then be used to create private virtual interfaces (VIFs) 
    to each AWS region.

    Direct Connect gateway provides a grouping of Virtual Private Gateways (VGWs) and Private Virtual Interfaces (VIFs) that belong to the same AWS account and 
    enables you to interface with VPCs in any AWS Region (except AWS China Region).

    You can share a private virtual interface to interface with more than one Virtual Private Cloud (VPC) reducing the number of BGP sessions required.

    AWS VPN CloudHub is not the best solution as you have been asked to implement high-bandwidth, low-latency connections and VPN uses the Internet so is not reliable.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   ALB supports authentication from OIDC compliant identity providers such as Google, Facebook and Amazon. It is implemented through an authentication 
    action on a listener rule that integrates with Amazon Cognito to create user pools.

    SAML can be used with Amazon Cognito but this is not the only option.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   For RTMP CloudFront distributions files must be stored in an S3 bucket.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   With Security Groups you can only assign permit rules, you cannot assign deny rules.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Application Load Balancer can distribute traffic to AWS and on-premise resources using IP addresses but cannot be used to distribute traffic in a weighted manner.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   A VPC automatically comes with a default network ACL which allows all inbound/outbound traffic. A custom NACL denies all traffic both inbound and outbound by default.

    Network ACL’s function at the subnet level and you can have permit and deny rules. Network ACLs have separate inbound and outbound rules and 
    each rule can allow or deny traffic.

    Network ACLs are stateless so responses are subject to the rules for the direction of traffic. NACLs only apply to traffic that is ingress or egress to 
    the subnet not to traffic within the subnet.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Connection draining is enabled by default and provides a period of time for existing connections to close cleanly. 
    When connection draining is in action an CLB will be in the status “InService: Instance deregistration currently in progress”.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   With ALB and NLB IP addresses can be used to register:

        Instances in a peered VPC.
        AWS resources that are addressable by IP address and port.
        On-premises resources linked to AWS through Direct Connect or a VPN connection.


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Route tables determine where network traffic is directed. In your route table, you must add a route for your remote network and 
    specify the virtual private gateway as the target. This enables traffic from your VPC that’s destined for your remote network to 
    route via the virtual private gateway and over one of the VPN tunnels. You can enable route propagation for your route table to automatically 
    propagate your network routes to the table for you.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   With Amazon CloudFront, you can enforce secure end-to-end connections to origin servers by using HTTPS. 
    Field-level encryption adds an additional layer of security that lets you protect specific data throughout system processing so that only certain applications can see it.

    Field-level encryption allows you to enable your users to securely upload sensitive information to your web servers. 
    The sensitive information provided by your users is encrypted at the edge, close to the user, and remains encrypted throughout your entire application stack. 
    This encryption ensures that only applications that need the data—and have the credentials to decrypt it—are able to do so.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   An egress-only Internet gateway is a horizontally scaled, redundant, and highly available VPC component that allows outbound communication over IPv6 
    from instances in your VPC to the Internet, and prevents the Internet from initiating an IPv6 connection with your instances.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   AWS Global Accelerator uses static IP addresses as fixed entry points for your application. 
    You can migrate up to two /24 IPv4 address ranges and choose which /32 IP addresses to use when you create your accelerator.

    This solution ensures the company can continue using the same IP addresses and they are able to direct traffic to the 
    application endpoint in the AWS Region closest to the end user. Traffic is sent over the AWS global network for consistent performance.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   You can manage a single connection for multiple VPCs or VPNs that are in the same Region by associating a Direct Connect gateway to a transit gateway. 
    The solution involves the following components:

        A transit gateway that has VPC attachments.
        A Direct Connect gateway.
        An association between the Direct Connect gateway and the transit gateway.
        A transit virtual interface that is attached to the Direct Connect gateway.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   A VPC endpoint for Amazon API Gateway can be created and this will provide access to API Gateway using private IP addresses and avoids the internet completely.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   DAX is a DynamoDB-compatible caching service that enables you to benefit from fast in-memory performance for demanding applications. 
    DAX addresses three core scenarios:

    As an in-memory cache, DAX reduces the response times of eventually consistent read workloads by an order of magnitude from single-digit milliseconds to microseconds.
    DAX reduces operational and application complexity by providing a managed service that is API-compatible with DynamoDB. 
    Therefore, it requires only minimal functional changes to use with an existing application.
    For read-heavy or bursty workloads, DAX provides increased throughput and potential operational cost savings by reducing the need to overprovision read capacity units. 
    This is especially beneficial for applications that require repeated reads for individual keys.



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   To enable outbound connectivity for instances in private subnets a NAT gateway can be created. The NAT gateway is created in a public subnet and 
    a route must be created in the private subnet pointing to the NAT gateway for internet-bound traffic. An internet gateway must be attached 
    to the VPC to facilitate outbound connections.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Per-client throttling limits are applied to clients that use API keys associated with your usage policy as client identifier. 
    This can be applied to the single customer that is issuing excessive API requests. This is the best option to ensure that only one customer is affected.

    Server-side throttling limits are applied across all clients. These limit settings exist to prevent your API—and your account—from being overwhelmed by too many requests. 
    In this case, the solutions architect need to apply the throttling to a single client.

    Per-method throttling limits apply to all customers using the same method. This will affect all customers who are using the API.

    Account-level throttling limits define the maximum steady-state request rate and burst limits for the account. This does not apply to individual customers.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   AWS Global Accelerator is a service in which you create accelerators to improve availability and performance of your applications for local and global users. 
    Global Accelerator directs traffic to optimal endpoints over the AWS global network. This improves the availability and performance of your internet applications 
    that are used by a global audience. Global Accelerator is a global service that supports endpoints in multiple AWS Regions, which are listed in the AWS Region Table.

    By default, Global Accelerator provides you with two static IP addresses that you associate with your accelerator. 
    (Or, instead of using the IP addresses that Global Accelerator provides, you can configure these entry points to be IPv4 addresses from 
    your own IP address ranges that you bring to Global Accelerator.)

    The static IP addresses are anycast from the AWS edge network and distribute incoming application traffic across multiple endpoint resources in multiple AWS Regions, 
    which increases the availability of your applications. Endpoints can be Network Load Balancers, Application Load Balancers, EC2 instances, 
    or Elastic IP addresses that are located in one AWS Region or multiple Regions.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   You can use Route 53 to check the health of your resources and only return healthy resources in response to DNS queries. 
    There are three types of DNS failover configurations:

    Active-passive: Route 53 actively returns a primary resource. In case of failure, Route 53 returns the backup resource. Configured using a failover policy.
    Active-active: Route 53 actively returns more than one resource. In case of failure, Route 53 fails back to the healthy resource. 
    Configured using any routing policy besides failover.
    Combination: Multiple routing policies (such as latency-based, weighted, etc.) are combined into a tree to configure more complex DNS failover.

    In this case an alias already exists for the secondary ALB. Therefore, the solutions architect just needs to enable a failover configuration 
    with an Amazon Route 53 health check.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   AWS Global Accelerator uses the vast, congestion-free AWS global network to route TCP and UDP traffic to a healthy application endpoint in the closest AWS Region 
    to the user.

    This means it will intelligently route traffic to the closest point of presence (reducing latency). Seamless failover is ensured as AWS Global Accelerator 
    uses anycast IP address which means the IP does not change when failing over between regions so there are no issues with client caches having incorrect entries 
    that need to expire.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   There are two different types of VPC endpoint: interface endpoint, and gateway endpoint. With an interface endpoint you use an ENI in the VPC. 
    With a gateway endpoint you configure your route table to point to the endpoint. Amazon S3 and DynamoDB use gateway endpoints. 
    This solution means that all traffic will go through the VPC endpoint straight to DynamoDB using private IP addresses.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   The only way to get this working is by using a VPC Security Group for the ELB that is configured to allow only the internal service IP ranges 
    associated with CloudFront. As these are updated from time to time, you can use AWS Lambda to automatically update the addresses. 
    This is done using a trigger that is triggered when AWS issues an SNS topic update when the addresses are changed.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   A VPG is used to setup an AWS VPN which you can use in combination with Direct Connect to encrypt all data that traverses the Direct Connect link. 
    This combination provides an IPsec-encrypted private connection that also reduces network costs, increases bandwidth throughput, 
    and provides a more consistent network experience than internet-based VPN connections.

    There is no option to enable IPSec encryption on the Direct Connect connection.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   AWS Transit Gateway connects VPCs and on-premises networks through a central hub. With AWS Transit Gateway, you can quickly add Amazon VPCs, AWS accounts, 
    VPN capacity, or AWS Direct Connect gateways to meet unexpected demand, without having to wrestle with complex connections or massive routing tables. 
    This is the operationally least complex solution and is also cost-effective.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Geolocation routing lets you choose the resources that serve your traffic based on the geographic location of your users, meaning the location that 
    DNS queries originate from.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   The MySQL clusters instances need to access a service on the internet. The most secure method of enabling this access with low operational overhead is 
    to create a NAT gateway. When deploying a NAT gateway, the gateway itself should be deployed in a public subnet whilst the route table in the private subnet 
    must be updated to point traffic to the NAT gateway ID.

    A virtual private gateway (VGW) is used with a VPN connection, not for connecting instances in private subnets to the internet.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   There are two architectures here that fulfill the requirement to create a web application that displays the data from the DynamoDB table.

    The first one is to use an API Gateway REST API that invokes an AWS Lambda function. A Lambda proxy integration can be used, 
    and this will proxy the API requests to the Lambda function which processes the request and accesses the DynamoDB table.

    The second option is to use an API Gateway REST API to directly access the sales performance data. 
    In this case a proxy for the DynamoDB query API can be created using a method in the REST API.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   Lambda@Edge is a feature of Amazon CloudFront that lets you run code closer to users of your application, which improves performance and reduces latency. 
    Lambda@Edge runs code in response to events generated by the Amazon CloudFront.

    You simply upload your code to AWS Lambda, and it takes care of everything required to run and scale your code with high availability at an 
    AWS location closest to your end user.

    In this case Lambda@Edge can compress the files before they are sent to users which will reduce data egress costs.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-   VPC sharing makes use of the AWS Resource Access Manager (AWS RAM) service. It enables the sharing of VPCs across accounts. In this model, 
    the account that owns the VPC (owner) shares one or more subnets with other accounts (participants) that belong to the same organization from AWS Organizations.

    This scenario could be implemented with Prod1 account as the VPC owner and the Prod2 account as a VPC participant. 
    This would allow the central control of the shared resource whilst enabling the EC2 instances in Prod2 to access the database.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
