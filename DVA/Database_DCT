- A Scan operation in Amazon DynamoDB reads every item in a table or a secondary index. By default, a Scan operation returns all of the data attributes for every item in the table or index. You can use the ProjectionExpression parameter so that Scan only returns some of the attributes, rather than all of them. 

---

- The BatchGetItem operation returns the attributes of one or more items from one or more tables.

---

- The maximum item size in DynamoDB is 400 KB, which includes both attribute name binary length (UTF-8 length) and attribute value lengths (again binary length). 

---

- Using the AWS Console, you are trying to scale DynamoDB past its pre-configured maximums. Which service limits can you increase by raising a ticket to AWS support?

---> Global Secondary Indexes per table 
---> Provisioned throughput limits

---

- When using Query, or Scan, DynamoDB returns all of the item attributes by default. To get just some, rather than all of the attributes, use a Projection Expression.

---

- DynamoDB Streams captures a time-ordered sequence of item-level modifications in any DynamoDB table and stores this information in a log for up to 24 hours. Applications can access this log and view the data items as they appeared before and after they were modified, in near-real time.


You can also use the CreateTable or UpdateTable API operations to enable or modify a stream. The StreamSpecification parameter determines how the stream is configured:


StreamEnabled — Specifies whether a stream is enabled (true) or disabled (false) for the table.


StreamViewType — Specifies the information that will be written to the stream whenever data in the table is modified:



KEYS_ONLY — Only the key attributes of the modified item.

NEW_IMAGE — The entire item, as it appears after it was modified.

OLD_IMAGE — The entire item, as it appeared before it was modified.

NEW_AND_OLD_IMAGES — Both the new and the old images of the item.

---

- “Use parallel scans” is incorrect as this will return results faster but place more burden on the table’s provisioned throughput.

- With parallel scans the Developer can maximize usage of the available throughput and have the scans distributed across the table’s partitions.


A parallel scan can be the right choice if the following conditions are met:



The table size is 20 GB or larger.

The table’s provisioned read throughput is not being fully used.

Sequential Scan operations are too slow.

---

- MEMCACHED



Simplest model and can run large nodes.

Can be scaled in and out and cache objects such as DBs.

Widely adopted memory object caching system.

Multi-threaded.


REDIS



Open-source in-memory key-value store.

Supports more complex data structures: sorted sets and lists.

Supports master / slave replication and multi-AZ for cross-AZ redundancy.

Support automatic failover and backup/restore.

---

- It is common to use key/value stores for storing session state data. The two options presented in the answers are Amazon DynamoDB and Amazon ElastiCache Redis. Of these two, ElastiCache will provide the lowest latency as it is an in-memory database.

---

- By default, the DynamoDB write operations (PutItem, UpdateItem, DeleteItem) are unconditional: Each operation overwrites an existing item that has the specified primary key. DynamoDB optionally supports conditional writes for these operations. A conditional write succeeds only if the item attributes meet one or more expected conditions. Otherwise, it returns an error. Conditional writes are helpful in many situations. For example, you might want a PutItem operation to succeed only if there is not already an item with the same primary key. Or you could prevent an UpdateItem operation from modifying an item if one of its attributes has a certain value.

---

- You can monitor Amazon DynamoDB using CloudWatch, which collects and processes raw data from DynamoDB into readable, near real-time metrics. These statistics are retained for a period of time, so that you can access historical information for a better perspective on how your web application or service is performing. By default, DynamoDB metric data is sent to CloudWatch automatically.


To determine how much of the provisioned capacity is being used you can monitor ConsumedReadCapacityUnits or ConsumedWriteCapacityUnits over the specified time period.

---

- Any delete operation will consume RCUs to scan/query the table and WCUs to delete the items. It will be much cheaper and simpler to just delete the table and recreate it again ahead of the next batch job. This can easily be automated through the API.

---

- DynamoDB uses the partition key’s value as an input to an internal hash function. The output from the hash function determines the partition in which the item is stored. Each item’s location is determined by the hash value of its partition key.


All items with the same partition key are stored together, and for composite partition keys, are ordered by the sort key value. DynamoDB splits partitions by sort key if the collection size grows bigger than 10 GB.


DynamoDB evenly distributes provisioned throughput—read capacity units (RCUs) and write capacity units (WCUs)—among partitions and automatically supports your access patterns using the throughput you have provisioned. However, if your access pattern  exceeds 3000 RCU or 1000 WCU for a single partition key value, your requests might be throttled with a ProvisionedThroughputExceededException error.


To avoid request throttling, design your DynamoDB table with the right partition key to meet your access requirements and provide even distribution of data. Recommendations for doing this include the following:



Use high cardinality attributes (e.g. email_id, employee_no, customer_id etc.)

Use composite attributes

Cache popular items

Add random numbers or digits from a pre-determined range for write-heavy use cases


In this case there is a hot partition due to the order date being used as the partition key and this is causing writes to be throttled. Therefore, the best solution to ensure the writes are more evenly distributed in this scenario is to add a random number suffix to the partition key values.

---

- BatchGetItem and BatchGetItem are used when you have multiple items to read/write.

---

- Scan operation is the least efficient way to retrieve the data as all items in the table are returned and then filtered. Pagination just breaks the results into pages.

---

- The BatchGetItem operation returns the attributes of one or more items from one or more tables. You identify requested items by primary key.


A single operation can retrieve up to 16 MB of data, which can contain as many as 100 items. In order to minimize response latency, BatchGetItem retrieves items in parallel.


By default, BatchGetItem performs eventually consistent reads on every table in the request. If you want strongly consistent reads instead, you can set ConsistentRead to true for any or all tables.

---

- Lazy Loading:

	Loads the data into the cache only when necessary (if a cache miss occurs).

	Lazy loading avoids filling up the cache with data that won’t be requested.

	If requested data is in the cache, ElastiCache returns the data to the application.

	If the data is not in the cache or has expired, ElastiCache returns a null.

	The application then fetches the data from the database and writes the data received into the cache so that it is available for next time.

	Data in the cache can become stale if Lazy Loading is implemented without other strategies (such as TTL).

Write Through:


	When using a write through strategy, the cache is updated whenever a new write or update is made to the underlying database.

	Allows cache data to remain up-to-date.

	Can add wait time to write operations in your application.

	Without a TTL you can end up with a lot of cached data that is never read.

---

- TransactWriteItems is a synchronous and idempotent write operation that groups up to 25 write actions in a single all-or-nothing operation. These actions can target up to 25 distinct items in one or more DynamoDB tables within the same AWS account and in the same Region. The aggregate size of the items in the transaction cannot exceed 4 MB. The actions are completed atomically so that either all of them succeed or none of them succeeds.


A TransactWriteItems operation differs from a BatchWriteItem operation in that all the actions it contains must be completed successfully, or no changes are made at all. With a BatchWriteItem operation, it is possible that only some of the actions in the batch succeed while the others do not.

---

- In addition to encryption at rest, which is a server-side encryption feature, AWS provides the Amazon DynamoDB Encryption Client. This client-side encryption library enables you to protect your table data before submitting it to DynamoDB. With server-side encryption, your data is encrypted in transit over an HTTPS connection, decrypted at the DynamoDB endpoint, and then re-encrypted before being stored in DynamoDB. Client-side encryption provides end-to-end protection for your data from its source to storage in DynamoDB.
